#ifndef PROTOCOLE
#define PROTOCOLE

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

	struct Capteur {
		char* id;
		uint32_t temperature;	//temperature en mK
		uint32_t pression;	//pression en Pa
		float vent;		//vitesse du vent en ms^-1
		uint16_t direction;	//direction en degre
		uint8_t humidite;	//taux d'humidite en pourcentage
		uint8_t type;		//type de nuage A->aucun, P->peu, E->epare, F->fragmente, C->couvert, H->haut, D->danger, I->inconnu, M->mort (la caste n'a pas d'importance)
		uint16_t plafond;	//altitude du plafond nuageux en m
	}; 
	typedef struct Capteur Capteur;

	typedef enum {
		true  = (1==1),
		false = (1==0),
	} bool_t;

	//FONCTIONS

	Capteur* init_capteur(void);
	char* capteur_concat(Capteur* C,char* id);
	Capteur* capteur_deconcat(char* message_recu);
	char* affichageCapteur(Capteur* C);
	char** mess_deconcat(char* text);
	char** liste_deconcat(char* text);
	uint8_t nb_ligne(char** text);
	double random_time(double a, int min, int max, int fluctuation_max, int valeur_centre);
	Capteur** capteur_aleatoire(int heures);
	Capteur* valeur_instantanee();
	Capteur* valeur_moyenne();
	Capteur* add_capteur(Capteur* a, Capteur* b);
	Capteur* div_capteur(Capteur* a, uint8_t b);


#endif
