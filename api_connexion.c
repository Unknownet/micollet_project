#include"api_connexion.h"
#define sortie_std stdout

Station* init_station(){
return init_station_id(NOM,ADDRESSE,PORT);
}

Station* init_station_id(char* id, char* adresse, uint16_t port){
	Station * Nouveau;
	Nouveau = (Station*)malloc(sizeof(Station));
	Nouveau->serveur = init_serveur();
	Nouveau->message_recu = (char*)calloc(150,sizeof(char));
	set_id(Nouveau,id);
	set_adress_p(Nouveau, adresse, port);
	Nouveau->flage_reception = false;
	Nouveau->connecte = false;
	Nouveau->stations = (char**)malloc(sizeof(char*));
	Nouveau->stations[0] = (char*)malloc(sizeof(char));
	Nouveau->stations[0][0] = '\0';
	sem_init(&(Nouveau->mutex),0,1);
	return Nouveau;
}

Serveur* init_serveur(){
	Serveur *Nouveau;
	Nouveau = (Serveur*)malloc(sizeof(Serveur));
	Nouveau->adresse = (char*)malloc(sizeof(char)*13);
	return Nouveau;
}

void set_station_serveur(Station* A,Serveur* B){
	strcpy(A->serveur->adresse, B->adresse);
	A->serveur->port = B->port;

}

void set_id(Station* A, char* identifiant){
	A->id = (char*)malloc(sizeof(char)*15);
	strcpy(A->id,identifiant);
}

void set_adress(Station* A, char* adresse){
	set_adress_p(A, adresse, 1101);
}

void set_adress_p(Station* A, char* adresse,uint16_t port){
	strcpy(A->serveur->adresse,adresse);
	A->serveur->port=port;
}

uint8_t envoi(Station* A, char* mess){
	if(A->connecte){
		send(A->c_socket,mess,sizeof(char)*150,0);
		return EXIT_SUCCESS;
	}
	return EXIT_FAILURE;
}

void* reception(void* A){
	char** tabText;
	char* text, *sav, *sav2, *sav3, *nom_hote, *message;
	uint8_t tmp,i,tmp_nb_station;

	while(((Station*)A)->connecte){
		if(recv(((Station*)A)->c_socket,((Station*)A)->message_recu,150,0)!=-1){
			//detection des requetes d'un clients et reenvoi vers ce client	ou affiche
			sem_wait(&((Station*)A)->mutex);
			if(((Station*)A)->message_recu[0]=='T'){
				text = (char*)malloc(sizeof(char)*150);
				((Station*)A)->flage_reception = false;
				strcpy(text,((Station*)A)->message_recu);
				strtok_r(text,"/",&sav);
				nom_hote = strtok_r(NULL,"/",&sav);
				nom_hote = strtok_r(nom_hote,"|",&sav2);
				strcpy(text,((Station*)A)->message_recu);
				strtok_r(text,"|",&sav3);
				strtok_r(NULL,"|",&sav3);
				message = strtok_r(NULL,"|",&sav3);
				if(message[0]=='r' || message[0]=='m'){
					//ENVOI
					tmp = nb_ligne(((Station*)A)->stations);
					tmp_nb_station = ((Station*)A)->num_station;
					for(i=0;i<tmp;i++){
						if(strcmp(((Station*)A)->stations[i],nom_hote)==0)
							((Station*)A)->num_station = i;
					}
					if(message[0]=='r'){
						transfert_donnee(((Station*)A), capteur_concat(valeur_instantanee(),((Station*)A)->id));
					}
					else if(message[0]=='m'){
						transfert_donnee(((Station*)A), capteur_concat(valeur_moyenne(),((Station*)A)->id));
					}
					((Station*)A)->num_station = tmp_nb_station;
	
				}
				else if(message[0]=='L'){
					//AFFICHE
					((Station*)A)->requete_recu = message;
				}
			}
			else{
				((Station*)A)->flage_reception = true;
			}
			sem_post(&((Station*)A)->mutex);
		}
	}
}

uint8_t login(Station* A){
	char mess[50];
	sprintf(mess,"C/%s\n",A->id);
	A->flage_reception = false;
	envoi(A, mess);
	while(!A->flage_reception);
	return strcmp(A->message_recu,"ACKC\n");
}

uint8_t connexion_login(Station* A){
	if(!connexion(A))
		return login(A);
	else {
		return EXIT_FAILURE;
	}
}

uint8_t connexion(Station* A){//ok

	int t, len;
	struct sockaddr_in remote;
	char str[100];
	
	if(!A->connecte){
		if ((A->c_socket = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
			perror("socket");
			A->connecte = false;
			return(EXIT_FAILURE);
		
		}

		printf("Trying to connect...\n");

		if (inet_aton(A->serveur->adresse, (struct in_addr *)&remote.sin_addr.s_addr) == -1) {
			perror("adresse");
			A->connecte = false;
			return(EXIT_FAILURE);
		}

		remote.sin_family = AF_INET;
		remote.sin_port = htons(A->serveur->port);

		len = sizeof(struct sockaddr_in);

		if (connect(A->c_socket, (struct sockaddr *)&remote, len) == -1) {
			perror("connect");
			A->connecte = false;
			return(EXIT_FAILURE);
		}
		else fprintf(sortie_std,"connexion effectue\n");
		A->connecte = true;
		if(pthread_create(&A->thread_reception, NULL, reception, (void*)A)==-1)
			fprintf(stderr,"probleme de thread !\n");
		return EXIT_SUCCESS;
	}
	else {
		fprintf(stderr,"erreur: vous etes deja connecte! \n");
		return EXIT_FAILURE;
	}
}

uint8_t deconnexion(Station* A){
	if(A->connecte){
		A->connecte = false;
		close(A->c_socket);
		return EXIT_SUCCESS;
	}
	return EXIT_FAILURE;
}

uint8_t liste(Station* A){
	uint8_t nb_stations;
	uint8_t tmp,i;
	char* liste;
	char** tabText;
	
	liste = (char*)malloc(sizeof(char)*150);
	A->flage_reception = false;
	envoi(A,"A\n");
	while(!A->flage_reception);
	strcpy(liste,A->message_recu);
	A->flage_reception = false;

	//effacer toutes les stations
	tmp = nb_ligne(A->stations);
	for(i=0;i<tmp;i++)
		free(A->stations[i]);
	free(A->stations);

	//reecrire les stations
	A->stations = liste_deconcat(liste);

	return EXIT_SUCCESS;
}

uint8_t requete_instantanee(Station* A){
	return transfert_donnee(A,"r");
}

uint8_t requete_moyenne(Station* A){
	return transfert_donnee(A,"m");
}

uint8_t transfert_donnee(Station* A, char* message){
	char* mess;
	uint8_t tmp;
	mess = (char*)malloc(sizeof(char)*200);
	sprintf(mess,"T/%s|%s|%s\n",A->id,A->stations[A->num_station],message);
	tmp = envoi(A,mess);
	free(mess);
	return tmp;
}




