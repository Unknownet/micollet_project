#include "protocole.h"

#define T_ALLOC 30

Capteur* init_capteur(void){//ok
	Capteur* C;
	C = (Capteur*)malloc(sizeof(Capteur));
	C->id = (char*)calloc(20,sizeof(char));
	strcpy(C->id,"Bodoni\0");
	C->temperature = 0;
	C->pression = 0;
	C->vent = -1.0;
	C->direction = 361;
	C->humidite = 101;
	C->type = 'M';
	C->plafond = 0;
	return C;
}

char* capteur_concat(Capteur* C, char* id){//ok
	char* chaine_concat,tmp;
	chaine_concat = (char*)malloc(150*sizeof(char));
	sprintf(chaine_concat,"L:%s;T:%d;P:%d;V:%f@%d;H:%d;N:%c@%d;X:void",
	id , C->temperature, C->pression, C->vent, C->direction, C->humidite, C->type, C->plafond);
	return chaine_concat;
}
	
Capteur* capteur_deconcat(char* message_recu){//ok

	uint8_t i;
	char *var1, *var2, *var3;
	char* tmp1, *tmp2, *tmp3;
	char** text;
	Capteur* capteur;

	capteur = init_capteur();
	text=(char**)calloc(7,sizeof(char*));
	for(i=0 ; i<7; i++ ){
		text[i]=(char*)calloc(T_ALLOC,sizeof(char));
	}
	var1 = (char*)calloc(T_ALLOC,sizeof(char));
	var2 = (char*)calloc(T_ALLOC,sizeof(char));
	var3 = (char*)calloc(T_ALLOC,sizeof(char));

	text[0] = strtok_r(message_recu,";",&tmp1);
	for(i=1 ; i<7; i++ ){
		strcpy(text[i],strtok_r(NULL,";",&tmp1));
	}

	for(i=0; i<7; ++i){

		strcpy(var1,strtok_r(text[i],":",&tmp2));
		strcpy(var2,strtok_r(NULL,":",&tmp2));

		switch(var1[0]){
		case 'L':
			{
		 	strcpy(capteur->id,var2);
			break;
			}
		case 'T': 
			{
			capteur->temperature = atoi(var2);
			break;
			}
		case 'P':
			{
			capteur->pression = atoi(var2);
			break;
			}
		case 'V':
			{
			strcpy(var3,var2);
			capteur->vent = atof(strtok_r(var3,"@",&tmp3));
			capteur->direction = atoi(strtok_r(NULL,"@",&tmp3));
			break;
			}
		case 'H':
			{
			capteur->humidite = atoi(var2);
			break;
			}
		case 'N':
			{
			strcpy(var3,var2);
			capteur->type = (strtok_r(var3,"@",&tmp3))[0];
			capteur->plafond = atoi(strtok_r(NULL,"@",&tmp3));
			break;
			}
		case 'X':
			{
			break;
			}
		default: ;break;
		}
	}

	return capteur;
}

char** mess_deconcat(char* text){
	char** tabText;
	char* tmp;
	char* sav;

	tmp = (char*)malloc(sizeof(char)*50);
	strcpy(tmp,text);
	tabText = (char**)malloc(sizeof(char*)*2);
	tabText[0] = (char*)malloc(sizeof(char)*2);
	strcpy(tabText[0],strtok_r(tmp,"/",&sav));
	strcpy(tmp,strtok_r(NULL,"/",&sav));
	switch(tabText[0][0]){
		case 'A':;break;
		case 'C':
		case 'D':
			{
			tabText[1] = (char*)malloc(sizeof(char)*strlen(tmp));
			strcpy(tabText[1],tmp);
			break;
			}
		default :
			{
			tabText[1] = (char*)malloc(sizeof(char)*7);
			strcpy(tabText[1],"erreur\0");
			break;
			}
	}
	return tabText;
}

uint8_t nb_chaine_entre_separateur(char* chaine, char* separateur){
	uint8_t i=1;
	char* text, *text2;
	char* sav;
	text = (char*)malloc(sizeof(char)*150);
	text2 = (char*)malloc(sizeof(char)*15);
	strcpy(text,chaine);
	strcpy(text2,strtok_r(text,separateur,&sav));
	while(text2 != NULL){
		text2 = strtok_r(NULL,separateur,&sav);
		i++;
	}
	return i-1;
}

char** liste_deconcat(char* text){
	char** tabText;
	uint8_t tmp,i;
	char* text_tmp, *text_tmp2;
	char* sav1, *sav2, *sav3;

	text_tmp = (char*)malloc(sizeof(char)*150);
	text_tmp2 = (char*)malloc(sizeof(char)*150);
	strcpy(text_tmp,text);
	strtok_r(text_tmp,"/",&sav1);
	strcpy(text_tmp2,strtok_r(NULL,"/",&sav1));
	
	tmp = nb_chaine_entre_separateur(text_tmp2,"|");
	tabText = (char**)malloc(sizeof(char*)*(tmp+1));
	for(i=0;i<tmp;i++){
		tabText[i] = (char*)malloc(sizeof(char)*15);	
	}
	tabText[0] = strtok_r(text_tmp2,"|",&sav2);
	tabText[0] = strtok_r(tabText[0],"\n",&sav3);
	for(i=1;i<tmp;i++){
		tabText[i] = strtok_r(NULL,"|",&sav2);
		tabText[i] = strtok_r(tabText[i],"\n",&sav3);
	}
	tabText[tmp] = (char*)malloc(sizeof(char));
	tabText[tmp][0]='\0';
	return tabText;
}

uint8_t nb_ligne(char** text){
	uint8_t tmp=0;
	while(text[tmp][0]!='\0'){
		tmp++;
	}	
	return tmp;
}

char* affichageCapteur(Capteur* C){//ok
	char* affichage, *type;
	type = (char*)malloc(sizeof(char)*15);	
	affichage = (char*)malloc(sizeof(char)*150);
	switch(C-> type){
		case 'A': type = strcpy(type,"aucun\0");break;
		case 'P': type = strcpy(type,"peu\0") ;break;
		case 'E': type = strcpy(type,"epars\0");break;
		case 'F': type = strcpy(type,"fragmenter\0");break;
		case 'C': type = strcpy(type,"couvert\0");break;
		case 'H': type = strcpy(type,"haut\0");break;
		case 'D': type = strcpy(type,"danger\0");break;
		case 'I': type = strcpy(type,"inconnu\0");break;
		case 'M': type = strcpy(type,"mort\0");break;
		default: type = strcpy(type,"inconnu\0");break;
	}


	sprintf(affichage,"\nNom de la station: %s \nTemperature: %'.2f C°\nPressions: %'.2f hPa\nVitesse du vent: %'.2f km/h, direction %d degre\nTaux d'humidite: %d %%\nHauteur de plafond: %d m, et de type: %s\n",C->id,(((double)C->temperature-273150.0)/1000),(((double)C->pression)/100),((double)C->vent*0.06), C->direction,C->humidite, C->plafond,type);

	return affichage;
}


double random_time(double a, int min, int max, int fluctuation_max, int valeur_centre){
	srand ( time(NULL) );
	double b,c,tmp;
	b = (rand()%(max-min))+min;
	c = a+(b/((double)(max-min))-0.5)*fluctuation_max;
	c = (c<min) ? (double)min:c;
	c = (c>max) ? (double)max:c;
	if(valeur_centre == 0){
		return c;
	}
	else {
		return (double)((2*c+valeur_centre)/3);
	}
}

Capteur** capteur_aleatoire(int heures){
	Capteur** temp_mesurer;
	int tmp,i;
	
	temp_mesurer = (Capteur**)malloc(heures*sizeof(Capteur*));
	for(i=0;i<heures;i++){
	temp_mesurer[i] = init_capteur();
	}

	i=0;
	while(i<heures){
		(temp_mesurer[i])->temperature = (uint32_t)random_time((double)(temp_mesurer[(i==0)?0:(i-1)])->temperature,243150,323150,10000,0);
		(temp_mesurer[i])->pression = (uint32_t)random_time((double)(temp_mesurer[(i==0)?0:(i-1)])->temperature,50000,800000,30000,100000);
		(temp_mesurer[i])->vent = (float)random_time((double)(temp_mesurer[(i==0)?0:(i-1)])->vent,0,3170,335,530);
		(temp_mesurer[i])->direction = (uint16_t)random_time((double)(temp_mesurer[(i==0)?0:(i-1)])->direction,0,360,10,0);
		(temp_mesurer[i])->humidite = (uint8_t)random_time((double)(temp_mesurer[(i==0)?0:(i-1)])->humidite,0,100,10,35);
		(temp_mesurer[i])->plafond = (uint16_t)random_time((double)(temp_mesurer[(i==0)?0:(i-1)])->plafond,800,2000,150,1000);
		tmp = rand()%16;
		switch(tmp){
			case 0: case 1: case 2: case 3: case 4: (temp_mesurer[i])->type = 'A'; break;
			case 5:case 6:case 7: (temp_mesurer[i])->type = 'P'; break;
			case 8: (temp_mesurer[i])->type = 'E'; break;
			case 9: case 10: (temp_mesurer[i])->type = 'F'; break;
			case 11: (temp_mesurer[i])->type = 'C'; break;
			case 12: case 13: (temp_mesurer[i])->type = 'H'; break;
			case 14: (temp_mesurer[i])->type = 'D'; break;
			case 15: (temp_mesurer[i])->type = 'I'; break;
			case 16: (temp_mesurer[i])->type = 'M'; break;
			default: (temp_mesurer[i])->type = 'I'; break;
		}
		i++;
	}
	return temp_mesurer;
}

Capteur* valeur_instantanee(){
	return	capteur_aleatoire(5)[4];
}

Capteur* add_capteur(Capteur* a, Capteur* b){
	Capteur* c;
	c = (Capteur*)malloc(sizeof(Capteur));
	c->temperature = a->temperature + b->temperature;
	c->pression = a->pression + b->pression;
	c->vent = a->vent + b->vent;
	c->direction = a->direction + b->direction;
	c->humidite = a->humidite + b->humidite;
	c->plafond = a->plafond + b->plafond;
	c->type = 'I';
	return c;
}

Capteur* div_capteur(Capteur* a, uint8_t b){
	Capteur* c;
	c = (Capteur*)malloc(sizeof(Capteur));
	c->temperature = a->temperature/b;
	c->pression = a->pression/b;
	c->vent = a->vent/b;
	c->direction = a->direction/b;
	c->humidite = a->humidite/b;
	c->plafond = a->plafond/b;
	c->type = 'I';
	return c;
}

Capteur* valeur_moyenne(){
	uint8_t i;
	Capteur** temp_mesurer;
	Capteur* c;
	temp_mesurer = capteur_aleatoire(10);
	c = temp_mesurer[0];
	for(i=1;i<10;i++){
		c = add_capteur(c,temp_mesurer[i]);
	}
	c = div_capteur(c,10);
	return c;	
}
