#include "protocole.h"
#include "api_connexion.h"

#include <string.h>
//#include <time.h>

/*PRINTF*/
#define clearscreen() printf("\033[2J")
#define efface() for(mlk=0;mlk<150;++mlk){printf("\033[K"); printf("\033[1B");}printf("\033[150A")
#define Noir 30
#define Blanc 37
#define Bleu 34
#define FondBlanc 47
#define FondNoir 40
#define Init 0

#define SaveCurseur() printf("\033[s")
#define RestoreCurseur() printf("\033[u")
#define initscreen() RestoreCurseur();efface();RestoreCurseur()
//#define couleur(param) "\033[" << param << "m"
//#define inverseCouleur couleur(FondNoir) << couleur(Blanc)

/*PANEL DE CONFIGURATION*/
#define ETAPE_CONNECTION 0
#define DEF_ADRESSE 1
#define MENU 2
#define TYPE_REQ 3


void drawTab(char** tableau){ //OK
	uint8_t i=1,y;
	uint8_t tmp,tmp1;
	printf("  ___________________________________________________\n\x2f\x2f                                                   \x5c\x5c\n");
	printf("||");
	tmp = 51 - strlen(tableau[0]);
	tmp1 = tmp%2;
	tmp /=2;
	for(y=0; y<tmp; y++)
		printf(" ");
	printf("%s",tableau[0]);
	for(y=0; y<(tmp+tmp1); y++)
		printf(" ");
	printf("||\n||");
	for(y=0; y<51; y++)
		printf(" ");
	printf("||\n");
	while(tableau[i][0]!= '\0'){
		printf("||   %d. ",i);
		tmp = 45 - strlen(tableau[i]);
		printf("%s",tableau[i]);
		for(y=0; y<tmp; y++)
			printf(" ");
		printf("||\n");
		i++;
	}
	printf("||");
	for(y=0; y<51; y++)
		printf(" ");
	printf("||\n\x5c\x5c___________________________________________________\x2f\x2f\n");
}

uint8_t saisie_touche(int max){ //OK
	uint8_t touche = 0;
	while((touche<=0) || (touche>max)){
		printf("Veuillez saisir l'option souhaite\n");
		scanf("%d",(int*)&touche);
	}
	return touche;
}

uint8_t etape_connection(){ //OK
	uint8_t tmp;
	char** tableau;
	tableau = (char**)malloc(sizeof(char*)*3);
	for(tmp=0;tmp<3;tmp++)
		tableau[tmp]=(char*)malloc(sizeof(char)*48);
	tableau[3] = (char*)malloc(sizeof(char));
	tableau[3][0] = '\0';
	strcpy(tableau[0],"ETAPE DE CONNECTION\0");
	strcpy(tableau[1],"Utiliser le serveur par defaut.\0");
	strcpy(tableau[2],"Specifier une adresse de votre choix.\0");
	
	drawTab(tableau);
	switch(saisie_touche(2)){
		case 1:{
			return MENU;
		}
		case 2:{
			return DEF_ADRESSE;
		}
		default:{
			return ETAPE_CONNECTION;
		}		
	}
}

uint8_t def_addresse(Station* stn){ //OK
	int tmp;
	char** tableau;
	tableau = (char**)malloc(sizeof(char*)*5);
	for(tmp=0;tmp<4;tmp++)
		tableau[tmp]=(char*)malloc(sizeof(char)*48);
	tableau[4] = (char*)malloc(sizeof(char));
	tableau[4][0] = '\0';
	strcpy(tableau[0],"DEFINIR LE SERVEUR\0");
	while(1){
		sprintf(tableau[1],"Definir l\'adresse (actuelle:%s)\0",stn->serveur->adresse);
		sprintf(tableau[2],"Definir le port (actuellement:%d)\0",stn->serveur->port);
		strcpy(tableau[3],"Valider\0");
		drawTab(tableau);
		switch(saisie_touche(3)){
			case 1:{
				printf("Veuillez saisir l'adresse\n");
				scanf("%s",stn->serveur->adresse);
			break;
			}
			case 2:{
				printf("Veuillez saisir le port\n");
				scanf("%d",&tmp);
				stn->serveur->port = tmp;
			break;
			}
			case 3:{
				return MENU;
			}
		
		}
	}

}

uint8_t menu(Station* stn){ //TODO AFFICHER TOUS LES CLIENTS
	uint8_t tmp;
	char** tableau;
	uint8_t Nb_client=nb_ligne(stn->stations);
	char nom_station[15];

	if(stn->connecte){
		tableau = (char**)malloc(sizeof(char*)*(Nb_client+5));
		for(tmp=0;tmp<(Nb_client+4);tmp++)
			tableau[tmp]=(char*)malloc(sizeof(char)*48);
		tableau[Nb_client+4] = (char*)malloc(sizeof(char));
		tableau[Nb_client+4][0] = '\0';
		strcpy(tableau[0],"CHOISIR UN CLIENT\0");
		for(tmp=0;tmp<Nb_client;tmp++)
			sprintf(tableau[tmp+1],"%s\0",stn->stations[tmp]);
		strcpy(tableau[Nb_client+1],"Raffraichir la liste\0");
		strcpy(tableau[Nb_client+2],"Modifier le nom de la station\0");
		strcpy(tableau[Nb_client+3],"Changer de serveur\0");
		drawTab(tableau);
		tmp = saisie_touche(Nb_client+3);
	

		if(tmp>0 && tmp<=Nb_client){
			stn->num_station = tmp-1;
			return TYPE_REQ;
		}
		else if(tmp == (Nb_client+1)){
			liste(stn);
			return MENU;
		}
		else if(tmp == (Nb_client+2)){
			deconnexion(stn);
			return MENU;
		}
		else if(tmp == (Nb_client+3)){
			deconnexion(stn);
			return ETAPE_CONNECTION;
		}
		else{
			return MENU;
		}
	}
	else{
		tableau = (char**)malloc(sizeof(char*)*5);
		for(tmp=0;tmp<4;tmp++)
			tableau[tmp]=(char*)malloc(sizeof(char)*48);
		tableau[4] = (char*)malloc(sizeof(char));
		tableau[4][0] = '\0';
		strcpy(tableau[0],"CONFIGURATION DE VOTRE SATION\0");
		strcpy(tableau[2],"Valider\0");
		strcpy(tableau[3],"Changer de serveur\0");
		do{
			sprintf(tableau[1],"Choisir un nom (actuel:%s)\0",stn->id);
			drawTab(tableau);
			tmp = saisie_touche(3);
			switch(tmp){
				case 1: {
					printf("Veuillez saisir le nom de la station\n");
					scanf("%s",nom_station);
					strcpy(stn->id,nom_station);
					break;
				}
				case 2: {
					connexion(stn);
					login(stn);
					liste(stn);
					break;
				}
				case 3: {
					return ETAPE_CONNECTION;
				}
			}
		}while(!stn->connecte && tmp==2);
		return MENU;
	}
}

uint8_t type_req(Station* stn){ //TODO 
	uint8_t tmp;
	char** tableau;
	tableau = (char**)malloc(sizeof(char*)*4);
	for(tmp=0;tmp<4;tmp++)
		tableau[tmp]=(char*)malloc(sizeof(char)*48);
	tableau[4] = (char*)malloc(sizeof(char));
	tableau[4][0] = '\0';
	strcpy(tableau[0],"TYPE DE REQUETES\0");
	strcpy(tableau[1],"valeur instantanee\0");
	strcpy(tableau[2],"moyenne sur 24 heures\0");
	strcpy(tableau[3],"Revenir\0");
	drawTab(tableau);
	switch(saisie_touche(3)){
		case 1:{
			//demander les valeur instantanee
			requete_instantanee(stn);
			usleep(150000);
			printf("%s\n",affichageCapteur(capteur_deconcat(stn->requete_recu)));
			return MENU;
		}
		case 2:{
			//demander la moyenne
			requete_moyenne(stn);
			usleep(150000);
			printf("%s\n",affichageCapteur(capteur_deconcat(stn->requete_recu)));
			return MENU;
		}
		case 3:{
			return MENU;
		}
		default:{
			return TYPE_REQ;
		}
	}
}

int main() // OK
{
	uint8_t nb_menu = 0;
	uint8_t mlk;
	Station* stn;

	stn = init_station();

	while(1){
		switch(nb_menu){
			case ETAPE_CONNECTION:{
			nb_menu = etape_connection();
			break;
			}
			case DEF_ADRESSE:{
			nb_menu = def_addresse(stn);
			break;
			}
			case MENU:{
			nb_menu = menu(stn);
			break;
			}
			case TYPE_REQ:{
			nb_menu = type_req(stn);
			break;
			}
			default:;break;
		}
	}
	 return 0;
}

