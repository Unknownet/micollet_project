CC     = gcc -m32
#CFLAGS += -Wall -Wextra -Werror
#CFLAGS += -ansi -pedantic -posix
CFLAGS += -static
#CFLAGS += -g
#CFLAGS += -lpthread -D_REENTRANT
RM     = rm -f

all: proto api main.c
	$(CC) -o station_meteo main.c api_connexion.o protocole.o $(CFLAGS) -lpthread -D_REENTRANT

proto: protocole.c protocole.h
	$(CC) -c protocole.c $(CFLAGS)

api: api_connexion.c api_connexion.h
	$(CC) -c api_connexion.c $(CFLAGS) -lpthread -D_REENTRANT

#prog: main.c decoupe.o serveur.o affiche.o
#        $(CC) -o $@ $^

#decoupe.o: decoupe.c decoupe.h
#        $(CC) -c $< $(CFLAGS)

#serveur.o: serveur.c serveur.h decoupe.h affiche.h
#        $(CC) -c $< $(CFLAGS)
        
#affiche.o: affiche.c affiche.h
#        $(CC) -c $< $(CFLAGS)

iso: se43_1_fv_dg.iso
	qemu-system-x86_64 -M pc -hda se43_1_fv_dg.iso -m 192 -net nic,vlan=0 -net nic -net user,vlan=0,hostname=emu  -boot d

qemu: se43_1_fv_dg.iso
	qemu-system-x86_64-spice se43_1_fv_dg.iso -net nic -net user&

mount:
	sudo mount se43_1_fv_dg.iso ./mnt -o loop

umount:
	sync
	sudo umount ./mnt

.PHONY: clean

clean:
	$(RM) *.o *.c~ *.h~ makefile~

