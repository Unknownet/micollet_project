#ifndef CONNEXION
#define CONNEXION

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <semaphore.h>


#include"protocole.h"

#define NOM "Bodoni"
#define ADDRESSE "10.0.2.2\0"
#define PORT 1101

	//ARGUMENTS

	struct Serveur {
		char* adresse;
		uint16_t port;
	};
	typedef struct Serveur Serveur;

	struct Station {	
		char* id;
		Serveur* serveur;
		int c_socket;
		bool_t connecte;
		pthread_t thread_reception;
		sem_t mutex;
		char* message_recu;
		char* requete_recu;
		volatile bool_t flage_reception;
		char** stations;
		uint8_t num_station;
	};
	typedef struct Station Station;

	//FONCTIONS

	Station* init_station();
	Station* init_station_id(char* id, char* adresse, uint16_t port);
	Serveur* init_serveur();
	void set_station_serveur(Station* A,Serveur* B);
	void set_id(Station* A, char* id);		//initialise l'identifiant
	void set_adress(Station* A, char* adresse);	//initialise l'adresse
	void set_adress_p(Station* A, char* adresse, uint16_t porti);	//initialise l'adresse
	void set_adress_s(Station* A, Serveur B);	//initialise les parametre du serveur
	uint8_t envoi(Station* A, char* mess);	//fonction communication type netcat
	uint8_t login(Station* A); //s'identifie au serveur
	void* reception(void* A);
	uint8_t connexion(Station* A);	//se connecter au serveur, retourne 0 success ou autre echec
	uint8_t connexion_login(Station* A);	//se connecter au serveur, retourne 0 success ou autre echec
	uint8_t deconnexion(Station* A);	//se deconnecter au serveur, retourne 0 success ou autre echec
	uint8_t liste(Station* A);		//liste les hotes
	uint8_t requete_instantanee(Station* A);
	uint8_t requete_moyenne(Station* A);
	uint8_t transfert_donnee(Station* A, char* message); //transfert les donnee en passant par un serveur passerelle

#endif
