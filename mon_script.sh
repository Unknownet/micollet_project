#!/bin/bash

echo "Ce scipt lance les deux images de la station meteo, ainsi que le serveur qui leur est associe"
echo "les images ne sont pas securise au niveau de la saisie des touche, aussi il faut faire attention a bien saisir les bonnes touches pour ne pas creer de defaillance systeme"
echo "pour appliquer les options des differentes fenetres veuillez ecrire dans le prompt le numero devant chaque option"
echo "le serveur de diffusion sera execute sur ce terminal"

echo "lancement de l image station meteo 1"
qemu ./image_station_1.img -net nic -net user &

echo "lancement de l image station meteo 2"
qemu ./image_station_2.img -net nic -net user &

echo "lancement du serveur de diffusion"
./serveur_diffusion
